package server

import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	"gitee.com/zuokuan/zrpc/client"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/util"
	"log"
	"net/http"
	"net/rpc"
	"time"
)

func Run(port int, plugins ...zrpc.PluginInterface) error {
	for _, plugin := range plugins {
		rpc.Register(plugin)
	}
	// 2.服务处理绑定到http协议上
	rpc.HandleHTTP()
	return http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

func RunForApi(port int, plugin *zrpc.Plugin) error {
	_const.CenterPort = port
	registerToCenter(port, plugin)
	rpc.Register(plugin)
	// 2.服务处理绑定到http协议上
	rpc.HandleHTTP()
	go serverCenterAliveValid(port, plugin)
	defer logoutToCenter(port, plugin)
	return http.ListenAndServe(fmt.Sprintf(":%d", plugin.AppInfo.Port), nil)
}

func registerToCenter(port int, plugin *zrpc.Plugin) {
	newClient, err := client.NewClient(port)
	if err != nil {
		log.Fatal(err)
		return
	}
	_, path, err := util.GetCurrentPath()
	if err != nil {
		log.Fatal(err)
		return
	}
	info := plugin.AppInfo
	callRequest := client.NewCallRequest("RegisterApp", info.AppName, info.AppDesc, path, info.Port, info.FunctionList)
	call := newClient.Call(callRequest)
	_, success, msg := call.Result()
	if !success {
		log.Fatal(msg)
	}
}

func logoutToCenter(port int, plugin *zrpc.Plugin) {
	newClient, err := client.NewClient(port)
	if err != nil {
		log.Fatal(err)
		return
	}
	info := plugin.AppInfo
	callRequest := client.NewCallRequest("Logout", info.AppName)
	call := newClient.Call(callRequest)
	_, success, msg := call.Result()
	if !success {
		log.Fatal(msg)
	}
}


func serverCenterAliveValid(port int, plugin *zrpc.Plugin){
	center, err := client.NewClient(port)
	if err != nil {
		log.Fatal(err)
		return
	}
	callRequest := client.NewCallRequest("Alive")
	for{
		call := center.Call(callRequest)
		_, success, msg := call.Result()
		if !success {
			log.Fatal(msg)
		}
		time.Sleep(time.Duration(5) * time.Second)
	}
}
