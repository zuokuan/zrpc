package server

import (
	"encoding/json"
	"gitee.com/zuokuan/zrpc"
	"gitee.com/zuokuan/zrpc/po"
	"gitee.com/zuokuan/zrpc/util"
	"log"
)

func NewSimplePlugin(n *pluginFunctions) *zrpc.Plugin {
	return &zrpc.Plugin{PluginFunctions: n.m}
}

func NewApiPlugin(AppName, AppDesc string, Port int, fs ...*zrpc.PluginFunction) *zrpc.Plugin {
	var functions []po.Function
	var functionMap = make(map[string]zrpc.PluginFunctionInterface)
	for _, f := range fs {
		if f.Name() == "" {
			log.Fatal("存在未定义函数名称的函数")
		}
		functions = append(functions, f.Info())
		functionMap[f.Name()] = f
	}
	functions = append(functions, logoutFunction.Info())
	functions = append(functions, aliveFunction.Info())
	functionMap[logoutFunction.Name()] = logoutFunction
	functionMap[aliveFunction.Name()] = aliveFunction
	marshal, err := json.Marshal(functions)
	if err != nil {
		log.Fatal(err)
	}
	var plugin = zrpc.Plugin{PluginFunctions: functionMap, AppInfo: po.AppInfo{AppName: AppName, AppDesc: AppDesc, Port: Port, FunctionList: string(marshal)}}
	return &plugin
}

type pluginFunctions struct {
	m map[string]zrpc.PluginFunctionInterface
}

func FunctionBuilder() *pluginFunctions {
	return &pluginFunctions{m: make(map[string]zrpc.PluginFunctionInterface)}
}

func (f *pluginFunctions) Add(funcName string, function zrpc.PluginFunctionInterface) *pluginFunctions {
	f.m[funcName] = function
	return f
}

func NewFunction() *zrpc.PluginFunction {
	return &zrpc.PluginFunction{}
}

func (f *pluginFunctions) AddForAPI(function zrpc.PluginFunctionInterface) *pluginFunctions {
	f.m[function.Name()] = function
	return f
}

func NewFunctionForApi(FunctionName, FunctionNameDesc, FunctionDesc string) *zrpc.PluginFunction {
	return zrpc.NewPluginFunction(FunctionName, FunctionNameDesc, FunctionDesc)
}


var logoutFunction = NewFunctionForApi("Logout", "登出服务", "公共服务,实现服务退出").
	SetArgs().
	Action(func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		go util.Logout()
		return nil
	})


var aliveFunction = NewFunctionForApi("Alive", "存活验证", "公共服务,实现服务存活验证").
	SetArgs().
	Action(func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		result.SuccessMsg("Alive!")
		return nil
	})