package client

import (
	"encoding/json"
	"gitee.com/zuokuan/zrpc"
	_const "gitee.com/zuokuan/zrpc/const"
)

var serverCenterClient *RpcClient

type ServerCenterClient struct {
}

func (*ServerCenterClient) ExecuteApp(AppName, FuncName string, args ...interface{}) (success bool, msg string, data map[string]interface{}, err error) {
	if serverCenterClient == nil {
		serverCenterClient, err = NewClient(_const.CenterPort)
		if err != nil {
			return false, "", nil, err
		}
	}
	marshal, _ := json.Marshal(args)
	request := executeAppRequest(AppName, FuncName, string(marshal))
	call := serverCenterClient.Call(request)
	result, s, m := call.Result()
	return s, m, result, nil
}

func (*ServerCenterClient) LogoutApp(AppName string) (success bool, msg string, data map[string]interface{}, err error) {
	if serverCenterClient == nil {
		serverCenterClient, err = NewClient(_const.CenterPort)
		if err != nil {
			return false, "", nil, err
		}
	}
	request := logoutAppRequest(AppName)
	call := serverCenterClient.Call(request)
	result, s, m := call.Result()
	return s, m, result, nil
}

func executeAppRequest(AppName, FuncName, args string) zrpc.CallRequest {

	return NewCallRequest("ExecuteApp", AppName, FuncName, args)
}

func logoutAppRequest(AppName string) zrpc.CallRequest {
	return NewCallRequest("LogoutApp", AppName)
}
