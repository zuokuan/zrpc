package client

import (
	"gitee.com/zuokuan/zrpc"
)


func NewCallRequest(funcName string, args ...interface{}) zrpc.CallRequest {
	return zrpc.CallRequest{FuncName: funcName, Args: args}
}



