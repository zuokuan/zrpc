package client

import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	"net/rpc"
)

var clients = make(map[string]*RpcClient)

func getClient(port int) (*RpcClient, error) {
	if clients[string(port)] == nil {
		http, err := rpc.DialHTTP("tcp", fmt.Sprintf(":%d", port))
		clients[string(port)] = &RpcClient{c: http}
		if err != nil {
			clients[string(port)] = nil
			return nil, err
		}
	}
	return clients[string(port)], nil
}

func RemoveClient(port int) {
	if clients[string(port)] == nil {
		return
	} else {
		clients[string(port)].c.Close()
		clients[string(port)] = nil
	}
}

type RpcClient struct {
	c *rpc.Client
}

func NewClient(port int) (client *RpcClient, err error) {
	return getClient(port)
}



func (c RpcClient) Call(request zrpc.CallRequest) *zrpc.CallResult {
	result := &zrpc.CallResult{}
	err := c.c.Call("Plugin.Call", request, &result)
	if err != nil {
		result.Error(err.Error())
		return result
	}
	if result.Code == "" {
		result.Code = "0"
	}
	return result
}

func (c RpcClient) TransCall(request zrpc.CallRequest, v interface{}) (success bool, msg string) {
	result := &zrpc.CallResult{}
	err := c.c.Call("Plugin.Call", request, &result)
	if err != nil {
		result.Error(err.Error())
	}
	_, success, msg = result.Result()
	if success {
		err = result.Trans(v)
		if err != nil {
			return false, "类型转换失败！"
		}
	}
	return
}
