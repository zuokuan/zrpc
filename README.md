# zrpc

#### 介绍

针对golang微服务实现构建RPC框架，基于TCP协议完成。
- 首发版本为v0.0.4，原因在之前的三次gitee发布中均以失败告终。
- 使用zrpc框架你无需关注服务的构建方式，只需要关注服务的定义和调用
- 当前版本v0.0.8（稳定版本）,中间版本增加了服务注册和默认退出的方法

#### 软件架构
软件架构说明


#### 安装教程

```go
    go get gitee.com/zuokuan/zrpc
```
获取最新版本
## 使用说明
----


### 以下为v0.0.8版本内容（稳定版本）

---
1、创建服务
```go
var AddFunction = server.NewFunction().SetArgs(
	po.NewArg(_const.Float64, "args1", "被加数"),
	po.NewArg(_const.Float64, "args2", "加数"),
).Action(
	func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		args1, _ := request.GetFloat64(0)
		args2, _ := request.GetFloat64(1)
		result.Success(map[string]interface{}{
			"sum": args1 + args2,
		})
		return nil
	},
)
var functions = server.FunctionBuilder().
	Add("Add", AddFunction)

var test = server.NewSimplePlugin(functions)

func main() {

	err := server.Run(28002, test)
	if err != nil {
		fmt.Println(err)
	}
}

```
注册到服务中心，后续发出来服务中心的代码。
```go
import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/po"
	"gitee.com/zuokuan/zrpc/server"
)

var AddFunction22 = server.
	NewFunctionForApi("Add", "加法", "两个参数的加法").
	SetArgs(
		po.NewArg(_const.Float64, "args1", "被加数"),
		po.NewArg(_const.Float64, "args2", "加数"),
	).Action(
	func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		args1, _ := request.GetFloat64(0)
		args2, _ := request.GetFloat64(1)
		result.Success(map[string]interface{}{
			"sum": args1 + args2,
		})
		return nil
	},
)

var testApi = server.NewApiPlugin(
	"Email", "邮件服务", 22001, AddFunction22)

func main() {
	err := server.RunForApi(22000, testApi)
	if err != nil {
		fmt.Println(err)
	}
}
```
2、客户端
```go
func main() {
	newClient, err := client.NewClient(28001)
	if err != nil {
		log.Fatal(err)
	}
	callRequest := client.NewCallRequest("Add", 100)
	data := Data{}
	call, msg := newClient.TransCall(callRequest, &data)
	if !call {
		fmt.Println(msg)
	}
	fmt.Println(data)

}
```


### 以下为v0.0.4版本内容

----

1. 创建服务
```go
package main

import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/po"
	"gitee.com/zuokuan/zrpc/server"
)

var AddFunction = server.NewFunction().SetArgs(
	po.NewArg(_const.INT,"args1","被加数"),
	po.NewArg(_const.INT,"args2","加数"),
).Action(
	func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		args1, _ := request.GetInt(0)
		args2, _ := request.GetInt(1)
		result.Success(map[string]interface{}{
			"sum": args1 + args2,
		})
		return nil
	},
)

var test = server.NewPlugin(
	server.FunctionBuilder().
		Add("Add", AddFunction),
)

func main() {
	err := server.Run(28001, test)
	if err != nil {
		fmt.Println(err)
	}
}
```
2. 客户端调用
```go
package main

import (
	"fmt"
	"gitee.com/zuokuan/zrpc/client"
	"log"
)

type Data struct {
	Sum int `json:"sum"`
}

func main() {
	newClient, err := client.NewClient(28001)
	if err != nil {
		log.Fatal(err)
	}
	callRequest := client.NewCallRequest("Add", 100, 200)
	data := Data{}
	newClient.TransCall(callRequest, &data)
	fmt.Println(data)

}

```


#### 参与贡献

