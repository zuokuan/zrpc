package main

import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/po"
	"gitee.com/zuokuan/zrpc/server"
)

var AddFunction22 = server.
	NewFunctionForApi("Add", "加法", "两个参数的加法").
	SetArgs(
		po.NewArg(_const.Float64, "args1", "被加数"),
		po.NewArg(_const.Float64, "args2", "加数"),
	).Action(
	func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		args1, _ := request.GetFloat64(0)
		args2, _ := request.GetFloat64(1)
		result.Success(map[string]interface{}{
			"sum": args1 + args2,
		})
		return nil
	},
)

var testApi = server.NewApiPlugin(
	"Email", "邮件服务", 22001, AddFunction22)

func main() {
	err := server.RunForApi(22000, testApi)
	if err != nil {
		fmt.Println(err)
	}
}
