package main

import (
	"fmt"
	"gitee.com/zuokuan/zrpc"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/po"
	"gitee.com/zuokuan/zrpc/server"
)

var AddFunction = server.NewFunction().SetArgs(
	po.NewArg(_const.Float64, "args1", "被加数"),
	po.NewArg(_const.Float64, "args2", "加数"),
).Action(
	func(request zrpc.CallRequest, result *zrpc.CallResult) error {
		args1, _ := request.GetFloat64(0)
		args2, _ := request.GetFloat64(1)
		result.Success(map[string]interface{}{
			"sum": args1 + args2,
		})
		return nil
	},
)
var functions = server.FunctionBuilder().
	Add("Add", AddFunction)

var test = server.NewSimplePlugin(functions)

func main() {

	err := server.Run(28002, test)
	if err != nil {
		fmt.Println(err)
	}
}
