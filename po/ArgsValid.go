package po

import (
	_const "gitee.com/zuokuan/zrpc/const"
)

//Arg 参数
type Arg struct {
	Type _const.ArgsType
	Name string
	Desc string
}

func NewArg(t _const.ArgsType, n string, d string) Arg {
	return Arg{Type: t, Name: n, Desc: d}
}
