package po

import _const "gitee.com/zuokuan/zrpc/const"

type AppInfo struct {
	AppName      string `json:"AppName"`
	AppDesc      string `json:"AppDesc"`
	Port         int    `json:"Port"`
	FunctionList string `json:"FunctionList"`
}

type Function struct {
	FunctionName     string        `json:"FunctionName"`
	FunctionNameDesc string        `json:"FunctionNameDesc"`
	FunctionDesc     string        `json:"FunctionDesc"`
	FunctionArgs     []FunctionArg `json:"FunctionArgs"`
}

type FunctionArg struct {
	ArgsName string          `json:"ArgsName"`
	ArgsType _const.ArgsType `json:"ArgsType"`
	ArgsDesc string          `json:"ArgsDesc"`
}
