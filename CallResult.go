package zrpc

import (
	"encoding/json"
	"fmt"
)

// CallResult 返回值
type CallResult struct {
	Code string                 `json:"code"` //0 成功 1失败
	Msg  string                 `json:"msg"`  //信息
	Data map[string]interface{} `json:"data"` //结果
}

func (cr *CallResult) Error(msg string, arg ...string) {
	cr.Msg = fmt.Sprintf(msg, arg)
	cr.Code = "1"
}

func (cr *CallResult) Success(data map[string]interface{}) {
	cr.Data = data
	cr.Code = "0"
}

func (cr *CallResult) SuccessMsg(msg string, arg ...string) {
	cr.Msg = fmt.Sprintf(msg, arg)
	cr.Code = "0"
}

func (cr *CallResult) AddData(key string, value interface{}) {
	cr.Data["key"] = value
}

func (cr *CallResult) Result() (data map[string]interface{}, success bool, msg string) {
	success = cr.Code == "0"
	msg = cr.Msg
	data = cr.Data
	return
}

func (cr *CallResult) Trans(v interface{}) error {
	marshal, err := json.Marshal(cr.Data)
	if err != nil {
		return err
	}

	json.Unmarshal(marshal, v)
	return nil
}
