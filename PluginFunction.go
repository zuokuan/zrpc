package zrpc

import (
	"errors"
	"fmt"
	"gitee.com/zuokuan/zrpc/po"
	"reflect"
)

// PluginFunctionInterface 插件方法接口
type PluginFunctionInterface interface {
	valid(request *CallRequest, result *CallResult) error
	execute(request CallRequest, result *CallResult) error
	Name() string
	Info() po.Function
}

type PluginFunction struct {
	functionInfo  po.Function
	argsValidList []po.Arg
	action        Action
}

var NewPluginFunction = func(FunctionName, FunctionNameDesc, FunctionDesc string) *PluginFunction {
	return &PluginFunction{
		functionInfo: po.Function{
			FunctionName:     FunctionName,
			FunctionNameDesc: FunctionNameDesc,
			FunctionDesc:     FunctionDesc,
		},
	}
}

func (p PluginFunction) Name() string {
	return p.functionInfo.FunctionName
}

func (p PluginFunction) Info() po.Function {
	return p.functionInfo
}

func (p PluginFunction) valid(request *CallRequest, result *CallResult) error {
	var args = request.Args
	if len(p.argsValidList) == 0 {
		return nil
	}
	if len(p.argsValidList) != len(args) {
		tips := fmt.Sprintf("参数校验失败：\n 入参个数为 %d , 实际入参个数为 %d \n", len(p.argsValidList), len(args))
		result.Error(tips)
		return errors.New(tips)
	}
	for i, valid := range p.argsValidList {
		targetType := reflect.TypeOf(args[i]).String()
		sourceType := string(valid.Type)
		if targetType != sourceType {
			if !(targetType == "float64" && sourceType == "int") {
				tips := fmt.Sprintf("参数校验失败：\n 参数下标【%d】 入参类型为 %s ，实际入参类型为 %s \n", i, sourceType, targetType)
				result.Error(tips)
				return errors.New(tips)
			}
		}
	}
	request.addValidArgs(p.argsValidList)
	return nil
}

func (p *PluginFunction) execute(request CallRequest, result *CallResult) error {
	return p.action(request, result)
}

func (p *PluginFunction) SetArgs(args ...po.Arg) *PluginFunction {
	p.argsValidList = args
	var functionArgs []po.FunctionArg
	for _, arg := range args {
		var t = po.FunctionArg{
			ArgsName: arg.Name,
			ArgsType: arg.Type,
			ArgsDesc: arg.Desc,
		}
		functionArgs = append(functionArgs, t)
	}
	p.functionInfo.FunctionArgs = functionArgs
	return p

}

func (p *PluginFunction) Action(action Action) *PluginFunction {
	p.action = action
	return p
}
