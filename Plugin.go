package zrpc

import (
	"errors"
	"gitee.com/zuokuan/zrpc/po"
)

// PluginInterface 插件接口
type PluginInterface interface {
	Call(request CallRequest, result *CallResult) error
}

type Plugin struct {
	AppInfo         po.AppInfo
	PluginFunctions map[string]PluginFunctionInterface
}

func (p Plugin) Call(request CallRequest, result *CallResult) error {
	function := p.PluginFunctions[request.FuncName]
	if function == nil {
		result.Error("方法%s不存在", request.FuncName)
		return errors.New(result.Msg)
	}
	err := function.valid(&request, result)
	if err != nil {
		return err
	}
	err = function.execute(request, result)
	if err != nil {
		return err
	}
	return nil
}


