package _const

type ArgsType string

const (
	STRING  ArgsType = "string"
	INT     ArgsType = "int"
	Float64     ArgsType = "float64"
	BOOLEAN ArgsType = "bool"
)
