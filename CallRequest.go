package zrpc

import (
	"errors"
	"fmt"
	_const "gitee.com/zuokuan/zrpc/const"
	"gitee.com/zuokuan/zrpc/po"
	"reflect"
)

// CallRequest 异步调用请求
type CallRequest struct {
	argsValidList []po.Arg
	FuncName      string        `json:"func_name"`
	Args          []interface{} `json:"args"`
}

func (c *CallRequest) addValidArgs(argsValidList []po.Arg) {
	c.argsValidList = argsValidList
}

func (c *CallRequest) GetString(index int) (str string, err error) {
	targetType := reflect.TypeOf(c.Args[index]).String()
	if c.argsValidList[index].Type != _const.STRING || targetType != "string" {
		err = errors.New(fmt.Sprintf("下标【%d】参数不能转换为string", index))
	}
	str = c.Args[index].(string)
	return
}

func (c *CallRequest) GetInt(index int) (i int, err error) {
	targetType := reflect.TypeOf(c.Args[index]).String()
	if c.argsValidList[index].Type == _const.INT && targetType == "int" {
		i = c.Args[index].(int)
	} else if c.argsValidList[index].Type == _const.INT && targetType == "float64" {
		i = int(c.Args[index].(float64))
	} else {
		err = errors.New(fmt.Sprintf("下标【%d】参数不能转换为int", index))
	}
	return
}

func (c *CallRequest) GetFloat64(index int) (i float64, err error) {
	targetType := reflect.TypeOf(c.Args[index]).String()
	if c.argsValidList[index].Type != _const.Float64 || targetType != "float64" {
		err = errors.New(fmt.Sprintf("下标【%d】参数不能转换为int", index))
	}
	i = c.Args[index].(float64)
	return
}

func (c *CallRequest) GetBool(index int) (b bool, err error) {
	targetType := reflect.TypeOf(c.Args[index]).String()
	if c.argsValidList[index].Type != _const.BOOLEAN || targetType != "bool" {
		err = errors.New(fmt.Sprintf("下标【%d】参数不能转换为bool", index))
	}
	b = c.Args[index].(bool)
	return
}
